package the_fireplace.everdark;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.WorldTickEvent;

public class FMLEvents {
	@SubscribeEvent
	public void worldTick(WorldTickEvent event){
		if(event.world.getWorldTime() > 21500)
			event.world.setWorldTime(14250);
	}
}
